import 'bootstrap/dist/css/bootstrap.min.css';
import { Cliente } from '../homebanking/Cliente';
import { Homebanking } from '../homebanking/Homebanking';

document.getElementById("btnAddMovCajaAhorro").addEventListener("click", () => {
    Cliente.addMovCajaAhorro(document.getElementById("documento-ca").value,
                        document.getElementById("concepto-ca").value,
                        document.getElementById("importe-ca").value,
                        document.getElementById("signo-ca").value);
    Homebanking.updateCurrentUser(document.getElementById("documento-ca").value);                        
    window.location.href = "movimientos.html";                        
});

document.getElementById("btnAddMovTarjetaCredito").addEventListener("click", () => {
    Cliente.addMovCreditCard(document.getElementById("documento-tc").value,
                        document.getElementById("concepto-tc").value,
                        document.getElementById("importe-tc").value,
                        document.getElementById("signo-tc").value);
    Homebanking.updateCurrentUser(document.getElementById("documento-tc").value);                                                
    window.location.href = "movimientos.html";                        
});