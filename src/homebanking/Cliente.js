import { Persona } from './Persona';
import { Homebanking } from './Homebanking';
import { CajaAhorro} from './CajaAhorro';
import { Prestamo } from './Prestamo';
import { TarjetaCredito } from './TarjetaCredito';

export class Cliente extends Persona {
    /*
        Propiedades de Persona
        Propiedades de Cliente
            requestCreditCard
            requestLoan 
            userName
            password
    */
    constructor(p_propsPersona, p_props) {
        super(p_propsPersona);
        this.cliente = p_props;
        this.estado = "Pendiente";
        this.requestLoan = false;
        this.cajaAhorro = new CajaAhorro(this.persona.numDocument).getCajaAhorro();
    }

    // Método que agrega un cliente al LocalStorage.
    add(){
        let colectionClientes = Homebanking.getColection('clients');
        let cliente = {
                firstName: this.persona.firstName,
                lastName: this.persona.lastName,
                birthDay: this.persona.birthDay,
                email: this.persona.email,
                numDocument: this.persona.numDocument,
                cliente: {
                    estado: this.estado,
                    userName: this.cliente.userName,
                    password: this.cliente.password,
                    motivo: this.cliente.motivo,
                    requestLoan: this.requestLoan,
                    cajaAhorro: this.cajaAhorro,
                    prestamos: [],
                    tarjetaCredito: {}
                }
            };
        colectionClientes.push(cliente);
        Homebanking.setColection('clients', colectionClientes);
    }

    // Método para aprobación o rechazo de cliente.
    static updateStateCliente(p_numDocument, p_state){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument;
        });
        colectionClientes[index].cliente.estado = p_state;
        Homebanking.setColection('clients', colectionClientes);
    }

    // Método para aceptar o rechazar un prestamo
    static updateStateLoan(p_numDocument, p_nroPrestamo, p_state){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument;
        });
        colectionClientes[index].cliente.requestLoan = colectionClientes[index].cliente.prestamos.filter(element => {
            return element.estado == "Pendiente";
        }).length == 1 ? false : true;
        let indexP = colectionClientes[index].cliente.prestamos.findIndex(element => {
            return element.nroPrestamo == p_nroPrestamo;
        });
        colectionClientes[index].cliente.prestamos[indexP].estado = p_state;
        Homebanking.setColection('clients', colectionClientes);
    }

    // Método para aceptar o rechazar una tarjeta de crédito
    static updateStateCreditCard(p_numDocument, p_state){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument;
        });
        colectionClientes[index].cliente.tarjetaCredito.estado = p_state;
        Homebanking.setColection('clients', colectionClientes);
    }

    // Método para solicitar una tarjeta de crédito
    static requestCreditCard(p_numDocument, p_motivo){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument && Object.values(element.cliente.tarjetaCredito).length == 0;
        });
        if (index != -1){
            colectionClientes[index].cliente.requestCreditCard = true;
            colectionClientes[index].cliente.tarjetaCredito = new TarjetaCredito(p_numDocument, p_motivo).getTarjetaCredito();
            Homebanking.setColection('clients', colectionClientes);
            return true;
        } else{
            return false;
        };
    }

    // Método para solicitar un prestamo
    static requestLoan(p_prestamo){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_prestamo.numDocument;
        });
        colectionClientes[index].cliente.requestLoan = true;
        colectionClientes[index].cliente.prestamos.push(new Prestamo(p_prestamo).getPrestamo());
        Homebanking.setColection('clients', colectionClientes);
    }

    // Método para agregar movimientos de tarjeta de crédito
    static addMovCreditCard(p_numDocument, p_comercio, p_importe, p_signo){
        let colectionClientes = Homebanking.getColection('clients');
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument;
        });
        colectionClientes[index].cliente.tarjetaCredito.movimientosTarjetaCredito
                .push(TarjetaCredito.addMovimiento(
                        p_comercio,
                        parseFloat(p_importe).toFixed(2),
                        new Date().toISOString().replace(/T.*/,'').split('-').reverse().join('/'),
                        parseInt(p_signo)));
        colectionClientes[index].cliente.tarjetaCredito.saldo = 
            colectionClientes[index].cliente.tarjetaCredito.limit +
            TarjetaCredito.calcSaldo(colectionClientes[index].cliente.tarjetaCredito.movimientosTarjetaCredito);                        
        Homebanking.setColection('clients', colectionClientes);
    }

    // Méotodo que agrega un movimiento a la caja de ahorro
    static addMovCajaAhorro(p_numDocument,  p_concepto, p_importe, p_signo){
        let colectionClientes = Homebanking.getColection("clients");
        let index = colectionClientes.findIndex(element => {
            return element.numDocument == p_numDocument;
        });
        if (index != -1){
            colectionClientes[index].cliente.cajaAhorro.movimientosCajaAhorro
                .push(CajaAhorro.addMovimiento(p_concepto, 
                    parseFloat(p_importe).toFixed(2), 
                    new Date().toISOString().replace(/T.*/,'').split('-').reverse().join('/'),
                    parseInt(p_signo)));
            colectionClientes[index].cliente.cajaAhorro.saldo = CajaAhorro.calcSaldo(colectionClientes[index].cliente.cajaAhorro.movimientosCajaAhorro);                    
            Homebanking.setColection('clients', colectionClientes);                    
            return true; 
        } else {
            return false;
        }                                                                                      
    }
}