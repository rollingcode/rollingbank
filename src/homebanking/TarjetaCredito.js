export class TarjetaCredito {
    /*
        numDocument,
        numCreditCard,
        since,
        thru,
        ccv,
        limit,
        movimientos
    */
    constructor(p_numDocument, p_motivo){
        this.numDocument = p_numDocument;
        this.numTarjetaCredito = '4015-3233-' + Math.round(Math.random()*9999) + '-' + Math.round(Math.random()*9999);
        let today = new Date();
        let year = parseInt(today.getFullYear().toString().substr(-2));
        this.referenciaPago = Math.round(Math.random()*99999999);
        this.since = today.getMonth() + '/' + year;
        this.thru = today.getMonth() + '/' + (year+5);
        this.ccv = Math.round(Math.random()*999);
        this.limit = 50000;
        this.estado = "Pendiente";
        this.motivo = p_motivo;
        this.saldo = this.limit;
        this.movimientosTarjetaCredito = [];
    }

    // Método para obtener los datos en forma de objeto.
    getTarjetaCredito(){
        let creditCard = {
            numDocument: this.numDocument,
            numTarjetaCredito: this.numTarjetaCredito,
            since: this.since,
            thru: this.thru,
            ccv: this.ccv,
            limit: this.limit,
            estado: this.estado,
            motivo: this.motivo,
            saldo: this.saldo,
            referenciaPago: this.referenciaPago,
            movimientosTarjetaCredito: this.movimientosTarjetaCredito
        };
        return creditCard;
    }

    static addMovimiento(p_comercio, p_importe, p_fecha, p_signo){
        let movimiento = {
            comercio: p_comercio,
            importe: p_importe,
            fecha: p_fecha,
            signo: p_signo
        };
        return movimiento;
    }

    static calcSaldo(p_movimientos){
        let saldo = 0;
        p_movimientos.forEach(element => {
            saldo += element.importe * element.signo; 
        });
        return saldo;
    }
}
