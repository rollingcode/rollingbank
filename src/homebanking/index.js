import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css'
import { Cliente } from './Cliente';
import { Homebanking } from './Homebanking';

var current_user = Homebanking.getColection("current_user");

// Muestra los datos de la caja de ahorro.
document.getElementById("cda").innerHTML= `Caja de ahorro: ${current_user.cliente.cajaAhorro.nroCuenta}`;
document.getElementById("montocaja").innerHTML= `<b>$${current_user.cliente.cajaAhorro.saldo}</b>`;

// Muestra los datos de la tarjeta de crédito del usuario logeado (current_user)
if (Object.values(current_user.cliente.tarjetaCredito).length != 0){
    if (current_user.cliente.tarjetaCredito.estado == "Aprobado"){
        let ultNumTarjeta = current_user.cliente.tarjetaCredito.numTarjetaCredito.split("-");
        document.getElementById("numCreditCard").innerHTML= `XXXX-XXXX-XXXX-${ultNumTarjeta[3]}`;
        document.getElementById("since").innerHTML= `<div>
                                                        <p class="ml-3 mb-0 w-100">DESDE</p>
                                                        <p class="ml-3 w-100">SINCE</p>
                                                    </div>
                                                    <div>
                                                        <p class="row ml-3">${current_user.cliente.tarjetaCredito.since}</p>
                                                    </div>`;
        document.getElementById("thru").innerHTML= `<div>
                                                        <p class="ml-3 mb-0 w-100">HASTA</p>
                                                        <p class="ml-3 w-100">THRU</p>
                                                    </div>
                                                    <div>
                                                        <p class="row ml-3">${current_user.cliente.tarjetaCredito.thru}</p>	
                                                    </div>`;
        document.getElementById("name").innerHTML= `${current_user.firstName} ${current_user.lastName}`;
        document.getElementById("ccv").innerHTML = current_user.cliente.tarjetaCredito.ccv;      
        document.getElementById("saldoTarjeta").innerHTML = `<div class="w-100 ml-0">
                                                        <p class="row justify-content-center font-weight-bold m-auto pb-3 saldoDisponible">Saldo disponible para compra</p>
                                                        <p class="row justify-content-center text-success saldoTarjeta font-weight-bold">
                                                           $ ${current_user.cliente.tarjetaCredito.saldo}</p>
                                                    </div>`; 
    } else {
        document.getElementById("estado-tarjeta").innerHTML = current_user.cliente.tarjetaCredito.estado;
    };    
} else {
    document.getElementById("contenedorizq2").innerHTML = `<h4 class="text-center py-5 titulo-prestamos bg-success text-light" id="prestamosDisponibles">
                                                                        No hay una tarjeta solicitada</h4>`;
};

// Muestra los datos de préstamos del usuario logeado (current_user)
if (current_user.cliente.prestamos.length == 0){
    document.getElementById("contenedorder").innerHTML = `<h4 class="text-center py-5 titulo-prestamos bg-success text-light m-2">
                                                                        No hay prestamos solicitados</h4>`;
}

current_user.cliente.prestamos.forEach(function (element, index){
    document.getElementById("contenedorder").innerHTML+= `
            
            <div class="contenedorder2 m-2 rounded">
                <h2 class="ml-3 mt-3"><a class="cajaAhorro" href="prestamos.html?element=${index}">Prestamo ${element.nroPrestamo}</h2></a>
                <h2 class="ml-3 mt-3">Estado: ${element.estado}</h2>
                <p class="ml-3 monto">${element.estado == 'Aprobado' ? 
                    'Saldo adeudado: <b>$ ' + parseFloat(element.saldo).toFixed(2) + '</b>' :
                    ''}</p>
            </div>
        `;
});

// Actualiza el CFT de acuerdo a la cantidad de cuotas seleccionadas en el prestamo
var interes = 0;
document.getElementById("cant-cuotas").addEventListener("blur", () => {
    let cantCuotas = document.getElementById("cant-cuotas").value;
    if (cantCuotas == "6")
        interes = 45;
    if (cantCuotas == "12")
        interes = 50;
    if (cantCuotas == "18")
        interes = 55;
    if (cantCuotas == "24")
        interes = 60;
    if (cantCuotas == "48")
        interes = 80;
    document.getElementById("interes").innerHTML = `<p>CFT: ${interes}% </p>`;
});

// Evento que realiza el pedido de un préstamo.
document.getElementById("btnSolicitarPrestamo").addEventListener("click", () => {
    let prestamo = {
        numDocument: current_user.numDocument,
        montoPrestado: parseInt(document.getElementById("monto-pedido").value),
        interes: parseInt(interes),
        fechaPrestamo: new Date(),
        cantCuotas: parseInt(document.getElementById("cant-cuotas").value),
        motivo: document.getElementById("motivo-prestamo").value
    };
    Cliente.requestLoan(prestamo);
    Homebanking.updateCurrentUser(current_user.numDocument);
    swal("Muchas gracias!", "Le llegará un email con la confirmación de su solicitud del préstamo.", "success")
    .then(() => {
        window.location.href = "homebanking.html";
    });    
});

// Evento que realiza el pedido de una tarjeta de crédito
document.getElementById("btnSolicitarTarjeta").addEventListener("click", () => {
    Cliente.requestCreditCard(current_user.numDocument, document.getElementById("motivo-tarjeta").value) ?
        swal("Muchas gracias!", "Le llegará un email con la confirmación de su solicitud del préstamo.", "success")
        .then(() => {
            Homebanking.updateCurrentUser(current_user.numDocument);
            window.location.href = "homebanking.html";
        }) :
        swal("Error!", "Ya posee una tarjeta activa", "error")
        .then(() => {
            Homebanking.updateCurrentUser(current_user.numDocument);
            window.location.href = "homebanking.html";
        });
});

// Evento que cierra la sesión de un usuario
document.getElementById("btnLogOut").addEventListener("click", () => {
    Homebanking.logOutUser();
    window.location.href = "index.html";
});