import { Homebanking } from '../homebanking/Homebanking';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';

var current_user = Homebanking.getColection("current_user");

document.getElementById("num-ca").innerHTML = current_user.cliente.cajaAhorro.nroCuenta;
document.getElementById("saldo-ca").innerHTML = `$ ${current_user.cliente.cajaAhorro.saldo}`;

document.getElementById("tabla").innerHTML = "";
current_user.cliente.cajaAhorro.movimientosCajaAhorro.forEach(function (element){
	document.getElementById("tabla").innerHTML += `
		<tr>
	        <th scope="row">${element.fecha}</th>
	        <td>${element.concepto}</td>
	        <td class=${element.signo == 1 ? "text-success" : "text-danger"}>${element.importe * element.signo}</td>
	    </tr>`;
});

// Evento que cierra la sesión de un usuario
document.getElementById("btnLogOut").addEventListener("click", () => {
    Homebanking.logOutUser();
    window.location.href = "index.html";
});