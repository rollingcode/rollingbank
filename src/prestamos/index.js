import { Homebanking } from '../homebanking/Homebanking';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';

var current_user = Homebanking.getColection("current_user");

let url = window.location.search;
let urlSplit = url.split("=");
let id = urlSplit[1];

if(current_user.cliente.prestamos[id].estado == "Aprobado"){
    document.getElementById("infop").innerHTML=`
    <p class="ml-3">Numero de prestamo: ${current_user.cliente.prestamos[id].nroPrestamo}</p>
    <p class="ml-3">Fecha aprobación: ${current_user.cliente.prestamos[id].fechaPrestamo}</p>
    <p class="ml-3">Fecha vencimiento: ${current_user.cliente.prestamos[id].fechaVencimiento}</p>
    <p class="ml-3 monto">Monto solicitado: <b>$${current_user.cliente.prestamos[id].saldo}</b></p>
    <p class="ml-3 monto">Saldo adeudado: <b>$${current_user.cliente.prestamos[id].montoAPagar}</b></p>
    <p class="ml-3">CFT: ${current_user.cliente.prestamos[id].interes}%</p>
    <p class="ml-3">Cantidad de cuotas: ${current_user.cliente.prestamos[id].cantCuotas}</p>`;

    current_user.cliente.prestamos[id].liqCuotas.forEach(function(element){
        document.getElementById("formulario").innerHTML += `
            <tr>
              <td scope="row">Nro. ${element.numCuota}</td>
              <td>$${element.montoPago}</td>
              <td>${element.estado}</td>
              <td>${element.diaPago}</td>
            </tr>`;
    });
} else {
    document.getElementById("infop").innerHTML = `<p class="text-danger font-weight-bold">Prestamo ${current_user.cliente.prestamos[id].estado}</p>`;
    document.getElementById("formulario").innerHTML = "";
};

// Evento que cierra la sesión de un usuario
document.getElementById("btnLogOut").addEventListener("click", () => {
    Homebanking.logOutUser();
    window.location.href = "index.html";
});