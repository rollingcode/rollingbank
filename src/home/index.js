import { Homebanking } from '../homebanking/Homebanking';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';
import { Cliente } from '../homebanking/Cliente';

// Limpia el current_user si quedó bugueado
Homebanking.removeColection('current_user');

//quitando .shake-bottom del chat
var chat = document.querySelector('.chat');

chat.addEventListener('click', function (){
    chat.classList.remove('shake-bottom');
});

// Botón de inicio de sesión
document.getElementById("btnLogin").addEventListener("click", () => {
    event.preventDefault;
    let numDocument = document.getElementById("documento").value;
    let userName = document.getElementById("usuario-ingresar").value;
    let password = document.getElementById("contraseña-ingresar").value;
    if (Homebanking.verifyUserLogin(numDocument, userName, password)) 
        window.location.href = "homebanking.html";
    else {
        swal("Error", "Los datos ingresados son incorrectos.", "error");  
    };              
});

// Botón de resitración
document.getElementById("register").addEventListener("click", () => {
    event.preventDefault;
    let persona = {
        firstName: document.getElementById("nombre-registrar").value,
        lastName: document.getElementById("apellido-registrar").value,
        birthDay: document.getElementById("fecha-nacimiento").value,
        numDocument: document.getElementById("documento-registrar").value,
        email: document.getElementById("email").value
    }
    let cliente = {
        motivo: document.getElementById("motivo").value,
        userName: document.getElementById("usuario").value,
        password: document.getElementById("password").value
    }
    new Cliente(persona, cliente).add();
    swal("Muchas gracias!", "Le llegará un email con la confirmación de la cuenta.", "success")
    .then(() => {
        window.location.href = "index.html";
    });
});

// Funcionalidad blur para controlar que el DNI no sea el mismo que el de otro cliente
document.getElementById("documento-registrar").addEventListener("blur", () => {
    event.preventDefault;
    let element = document.getElementById("documento-registrar");
    if (Homebanking.verifyNumDocument(element.value)){
        element.style.background = "#e63142c2";
        element.setAttribute("data-content","El documento ingresado ya fue registrado para otro cliente");
        $('[data-toggle="popoverDoc"]').popover("show");
    } else{
        element.style.background = "";
        $('[data-toggle="popoverDoc"]').popover("hide");
    }; 
});

// Funcionalidad blur para controlar que el usuario ingresado no sea el mismo de otro cliente
document.getElementById("usuario").addEventListener("blur", () => {
    event.preventDefault;
    let element = document.getElementById("usuario");
    if (Homebanking.verifyUserName(element.value)){
        element.style.background = "#e63142c2";
        element.setAttribute("data-content","El nombre de usuario ya fue elegido por otro cliente");
        $('[data-toggle="popoverUser"]').popover("show");
    } else{
        element.style.background = "";
        $('[data-toggle="popoverUser"]').popover("hide");
    }; 
});

// Funcionalidad blur para controlar que las contraseña elegidas por el usuario sean las mismas.
document.getElementById("repetir-password").addEventListener("blur", () => {
    event.preventDefault;
    let password = document.getElementById("password");
    let passwordR = document.getElementById("repetir-password");
    if (password.value != passwordR.value) {
        passwordR.style.background = "#e63142c2";
        passwordR.setAttribute("data-content","Las constraseñas ingresadas no coinciden");
        $('[data-toggle="popoverPass"]').popover("toggle");
        passwordR.focus();
    } else {
        passwordR.style.background = "";
        $('[data-toggle="popoverPass"]').popover("hide");
    };
});

