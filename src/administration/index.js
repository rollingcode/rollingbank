import { Homebanking } from "../homebanking/Homebanking";
import { Cliente } from "../homebanking/Cliente";
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';

(function(){
    emailjs.init("user_CiKR8TTnCIvagsVr850tL");
 })();

showClientsPend();
showPrestamosPend();
showTarjetasCreditoPend();

// Muestra los clientes que están en estado Pendiente de aceptación 
function showClientsPend(){
    let clients = document.getElementById("clients");
    clients.innerHTML = '<p class="font-italic">No hay clientes pendientes de aprobación</p>';
    let messageEmail = "Queriamos informarle que su cuenta ha sido *valor* para operar en nuestro banco.";
    Homebanking.getClientsPend().forEach((element, index) => {
        if (index == 0)
            clients.innerHTML = "";
        clients.innerHTML += `<tr>
                                <th scope="row">${index + 1}</th>
                                <td>${element.firstName}</td>
                                <td>${element.lastName}</td>
                                <td>${element.numDocument}</td>
                                <td>${element.cliente.motivo}</td>
                                <td><button type="button" class="btn btn-success" id="btnValidarClient${index}">Validar</button></td>
                                <td><button type="button" class="btn btn-danger" id="btnRechazarClient${index}">Rechazar</button></td>
                            </tr>`;
        setTimeout(() => {    
            document.getElementById(`btnValidarClient${index}`).addEventListener("click", () => {
                Cliente.updateStateCliente(element.numDocument, "Aprobado");
                showClientsPend();
                messageEmail = messageEmail.replace("*valor*", "APROBADA");
                sendEmail(element.email, `${element.firstName} ${element.lastName}`, messageEmail);
                Homebanking.updateCurrentUser(element.numDocument);
            });
        }, 0);
        setTimeout(() => { 
            document.getElementById(`btnRechazarClient${index}`).addEventListener("click", () => {
                Cliente.updateStateCliente(element.numDocument, "Rechazado");
                showClientsPend();
                messageEmail = messageEmail.replace("*valor*", "RECHAZADA");                
                sendEmail(element.email, `${element.firstName} ${element.lastName}`, messageEmail);
                Homebanking.updateCurrentUser(element.numDocument);
            });  
        }, 0);              
    });

}

function showPrestamosPend(){
    let prestamos = document.getElementById("prestamos");
    prestamos.innerHTML = '<p class="font-italic">No hay préstamos pendientes de aprobación</p>';
    let messageEmail = "Queriamos informarle que su prestamo ha sido *valor*";
    Homebanking.getClientsPrestamosPend().forEach((element, index) => {
        if(index == 0)
            prestamos.innerHTML = "";
        prestamos.innerHTML += `<tr>
                                    <th scope="row">${index + 1}</th>
                                    <td>${element.firstName}</td>
                                    <td>${element.lastName}</td>
                                    <td>${element.numDocument}</td>
                                    <td id="prestamo${index}"></td>
                                </tr>`;
        let prestamo = document.getElementById(`prestamo${index}`);
        let destinatario = `${element.firstName} ${element.lastName}`;
        let emailDestinatario = element.email;
        prestamo.innerHTML = "";
        Homebanking.getPrestamosPend(element.numDocument).forEach((element, index) => {
            prestamo.innerHTML +=  `<p>Nro préstamo: <strong>${element.nroPrestamo}</strong></p>
                                    <p>Monto solicitado: <strong>${element.montoPrestado}</strong></p>
                                    <p>CFT: <strong>${element.interes}</strong></p>
                                    <p>Cuotas: <strong>${element.cantCuotas}</strong></p>
                                    <p>Motivo: <strong>${element.motivo}</strong></p>
                                    <div>
                                        <td><button type="button" class="btn btn-success" id="btnOtorgarPrestamo${index}">Otorgar</button></td>
                                        <td><button type="button" class="btn btn-danger" id="btnRechazarPrestamo${index}">Rechazar</button></td>
                                    </div>`;
            setTimeout(() => {
                document.getElementById(`btnOtorgarPrestamo${index}`).addEventListener("click", () => {
                    Cliente.updateStateLoan(element.numDocument, element.nroPrestamo, "Aprobado");
                    showPrestamosPend();    
                    messageEmail = messageEmail.replace("*valor*", "APROBADO. Felicitaciones!");
                    sendEmail(emailDestinatario, destinatario, messageEmail);
                    Homebanking.updateCurrentUser(element.numDocument);
                });
            }, 0);
            setTimeout(() => {
                document.getElementById(`btnRechazarPrestamo${index}`).addEventListener("click", () => {
                    Cliente.updateStateLoan(element.numDocument, element.nroPrestamo, "Rechazado");    
                    showPrestamosPend();
                    messageEmail = messageEmail.replace("*valor*", "RECHAZADO. Lo sentimos mucho!");
                    sendEmail(emailDestinatario, destinatario, messageEmail);
                    Homebanking.updateCurrentUser(element.numDocument);
                });
            }, 0);
        });
    });
}

function showTarjetasCreditoPend(){
    let tarjetas = document.getElementById("tarjetas");
    tarjetas.innerHTML = '<p class="font-italic">No hay tarjetas de crédito pendientes de aprobación</p>';
    let messageEmail = "Queriamos informarle que su pedido de tarjeta de crédito ha sido *valor*";
    Homebanking.getTarjetasCreditoPend().forEach((element, index) => {
        if(index == 0)
            tarjetas.innerHTML = "";
        tarjetas.innerHTML += `<tr>
                                <th scope="row">${index + 1}</th>
                                <td>${element.firstName}</td>
                                <td>${element.lastName}</td>
                                <td>${element.numDocument}</td>
                                <td>${element.cliente.tarjetaCredito.motivo}</td>
                                <td><button type="button" class="btn btn-success" id="btnOtorgarTarjetaCredito${index}">Otorgar</button></td>
                                <td><button type="button" class="btn btn-danger" id="btnRechazarTarjetaCredito${index}">Rechazar</button></td>
                            </tr>`;
        setTimeout(() => {
            document.getElementById(`btnOtorgarTarjetaCredito${index}`).addEventListener("click", () => {
                Cliente.updateStateCreditCard(element.numDocument, "Aprobado");
                showTarjetasCreditoPend();
                messageEmail = messageEmail.replace("*valor*", "APROBADO. Felicitaciones!");     
                sendEmail(element.email, `${element.firstName} ${element.lastName}`, messageEmail);   
                Homebanking.updateCurrentUser(element.numDocument);        
            });
        }, 0);
        setTimeout(() => {
            document.getElementById(`btnRechazarTarjetaCredito${index}`).addEventListener("click", () => {
                Cliente.updateStateCreditCard(element.numDocument, "Rechazado");
                showTarjetasCreditoPend();
                messageEmail = messageEmail.replace("*valor*", "RECHAZADO. Lo sentimos mucho!");
                sendEmail(element.email, `${element.firstName} ${element.lastName}`, messageEmail);
                Homebanking.updateCurrentUser(element.numDocument);
            });
        }, 0);
    });
    
}

// Envía un email al cliente 
function sendEmail(p_replyTo, p_toName, p_message){
    var params = {
        "reply_to": p_replyTo,
        "to_name": p_toName,
        "message_html": p_message
    };
    var service_id = "rollingbank-administrator";
    var template_id = "template_rolling";
    emailjs.send(service_id, template_id, params);
}